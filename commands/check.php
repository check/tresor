<?php

require_once("_repository.php");

class CheckCommand extends RepositoryCommand
{
    private function check()
    {
        $remote = $this->config('remote');
        $store = $this->config('store');
        $this->print_info("Retrieve contents of '".$remote."' to '".$store."'");

        $localBranches = $this->number_of_backups();
        // ignore 
        $remoteBranches = $this->execute(GIT." ls-remote --heads origin | grep -v master | wc -l");
        if ($localBranches != $remoteBranches)
        {
            echo "Repository can be updated (".($remoteBranches - $localBranches)." backups)- please run 'sync' command".PHP_EOL;
        }
        else
        {
            echo "Repository up to date".PHP_EOL;
        }
    }

    protected function run_in_repository()
    {
        if ($this->has('remote'))
        {
            $this->check();
        }
        else
        {
            throw new Exception( "Checking for updates only possible on mirror repository");
        }
    }
}
?>