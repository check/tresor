<?php

require_once("_repository.php");

class SyncCommand extends RepositoryCommand
{
    private $initialized;

    protected function visit_store()
    {
        parent::visit_store();
        $this->initialized = true;
        if (!$this->exists_in_store())
        {
            $this->initialized = false;
        }
    }

    protected function exists_in_store()
    {
        return !$this->initialized || parent::exists_in_store();
    }

    private function sync()
    {
        $remote = $this->config('remote');
        $store = $this->config('store');
        $this->print_info("Retrieve contents of '".$remote."' to '".$store."'");
        if (!$this->initialized)
        {
            $this->execute(GIT." clone ".$remote." ".$store);
            $this->execute(TOUCH." .git/BACKUP_REPOSITORY");
            $this->print_info("Initialized backup repositories \"".getcwd()."\"");
        }
        else
        {
           $this->execute("git pull");
        }
        $this->print_info("Syncronization completed");
    }

    protected function run_in_repository()
    {
        if ($this->has('remote'))
        {
            $this->sync();
            $this->compress_store();
        }
        else
        {
            throw new Exception( "Syncronization only possible on mirror repository");
        }
    }
}
?>