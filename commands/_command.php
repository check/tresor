<?php 

abstract class Command 
{
    private $configuration;

    public function __construct($configuration)
    {
        $this->configuration = $configuration;
    }
    
    public abstract function run();

    protected function print_info($info)
    {
        if ($this->config('verbose') == 1)
        {
            echo $info.PHP_EOL;
        }
    }

    protected function execute($command)
    {
        if ($this->config('verbose') == 2)
        {
            echo "Executing: \"".$command."\"".PHP_EOL;
        }
        if ($this->config('verbose') == 3)
        {
            $command." > /dev/null";
        }
        if ($this->config('try_run'))
        {
            echo "\t".$command.PHP_EOL;
        }
        else
        {
            return exec($command);
        }
    }

    protected function config($key, $default = null)
    {
        if ($this->has($key))
        {
            return $this->configuration[$key];
        }
        return $default;
    }

    protected function has($key)
    {
        return array_key_exists($key, $this->configuration);
    }
}
?>
