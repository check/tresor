<?php 

require_once("_repository.php");

class SnapshotCommand extends RepositoryCommand
{
    private function snapshot($backup)
    {
        $this->select_backup($backup);
        $this->execute(GIT." archive --format=zip -o ".$this->working_directory."/".$backup.".zip HEAD");
        $this->select_backup("master");
    }

    public function run_in_repository()
    {
        if (sizeof($this->config('arguments')) != 1)
        {
            throw new Exception("snapshot command needs backup to snapshot as arguments");
        }
        $this->snapshot($this->config('arguments')[0]);
    }
}
?>