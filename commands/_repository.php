<?php

require_once("_command.php");

abstract class RepositoryCommand extends Command
{
    protected $working_directory;

    public function __construct($configuration)
    {
        parent::__construct($configuration);
        $this->working_directory = getcwd();
    }

    protected function exists_in_store()
    {
        return is_dir(".git") && file_exists(".git/BACKUP_REPOSITORY");
    }

    protected function run_in_repository()
    {
        throw new Exception("Not implemented");
    }

    public function run()
    {
        $this->visit_store();
        if (!$this->exists_in_store())
        {
            throw new Exception("Repository not initialized in '".getcwd()."', please run 'sync' or 'backup' command");
        }
        else
        {
            $this->run_in_repository();
        }
    }

    public function __destruct()
    {
        chdir($this->working_directory);
    }

    protected function compress_store()
    {
        if ($this->config('compress'))
        {
            $this->print_info("Compressing backupes");
            $this->execute(GIT." reflog expire --expire=0 --all");
            $this->execute(GIT." prune --expire now");
            $this->execute(GIT." gc");
        }
    }

    protected function visit_store()
    {
        $directory = $this->config('store');
        if (!file_exists($directory) and !is_dir($directory)) {
            mkdir($directory);         
        } 
        chdir($directory);
        $this->print_info("Change working directory to '".$directory."'");
    }


    protected function number_of_backups()
    {
        return max(
            trim($this->execute(GIT." branch | grep -v master | wc -l")),
            trim($this->execute(GIT." branch -r | grep -v master | wc -l"))
        );
    }

    protected function get_oldest_backup()
    {
        $backups = $this->get_backups();
        // get last branch since it is the oldest
        return $backups[0];
    }

    private function parse_branches($command)
    {
        $temporary = "__branches.".time().".tmp";
        $this->execute($command." | grep -v master > ".$temporary);
        // $this->execute(GIT." branch -a | grep -v master  > ".$temporary);
        $branches = file_get_contents($temporary);
        unlink($temporary);
        $splitted = explode(" ", $branches);
        $branches = array();
        foreach ($splitted as $branch)
        {
            $branch = str_replace("*", "", $branch);
            $branch = trim($branch);
            $branch = str_replace("remotes/", "", $branch);
            $branch = str_replace("origin/", "", $branch);
            if (!empty($branch) && !in_array($branch, $branches))
            {
                $branches[] = $branch;
            }
        }
        return $branches;
    }

    private function get_local_backups()
    {
        return $this->parse_branches(GIT." branch");
    }

    private function get_remote_backups()
    {
        return $this->parse_branches(GIT." branch -r");
    }


    protected function get_backups()
    {
        $locals = $this->get_local_backups();
        $remotes = $this->get_remote_backups();
        $branches = array_merge($locals, $remotes);
        $branches = array_unique($branches);
        sort($branches);
        return $branches;
    }

    protected function select_backup($backup)
    {
        $locals = $this->get_local_backups();
        if ($backup != "master" && !in_array($backup, $locals))
        {
            $this->execute(GIT." checkout ".$backup);
        }
        else
        {
            $this->execute(GIT." checkout -f ".$backup);
        }
    }
}
?>