<?php

require_once("_repository.php");

class RestoreCommand extends RepositoryCommand
{
    private function restore($backup)
    {
        $this->select_backup($backup);
    }

    public function run_in_repository()
    {
        if (sizeof($this->config('arguments')) != 1)
        {
            throw new Exception("restore command needs backup to restore as argument");
        }
        $this->restore($this->config('arguments')[0]);
    }
}
?>