<?php

require_once("_repository.php");

class BackupCommand extends RepositoryCommand
{
    protected function run_in_repository()
    {
        $this->create_incremental_backup();
        $this->delete_obsolete_backups();
        $this->compress_store();
    }

    protected function visit_store()
    {
        parent::visit_store();
        if (!is_dir(".git") || !file_exists(".git/BACKUP_REPOSITORY"))
        {
            $this->execute(GIT." init");
            $this->execute(TOUCH." empty");
            $this->execute(TOUCH." .git/BACKUP_REPOSITORY");
            $this->execute(GIT." add empty");
            $this->execute(GIT." commit -m \"Initialized backup repository\"");
            $this->print_info("Initialized backup repositories \"".getcwd()."\"");
        }
        $this->execute(GIT." checkout -f master");
    }

    private function empty_temporary_storage($backupConfiguration)
    {
        return array_key_exists('empty_temporary_storage', $backupConfiguration)
            && $backupConfiguration['empty_temporary_storage'];
    }

    private function get_excludes($configuration)
    {
        $exclude = array();
        if (array_key_exists('exclude', $configuration))
        {
            $exclude = $configuration['exclude'];
        }
        if (!is_array($exclude))
        {
            $exclude = array($exclude);
        }
        return $exclude;
    }

    private function get_tables($configuration)
    {
        $connection = mysql_connect('localhost', $configuration['user'], $configuration['password']);
        mysql_select_db($configuration['database'], $connection);
        $result = mysql_query("SHOW TABLES");
        $tables = array();
        $excludes = $this->get_excludes($configuration);
        while ($fetched = mysql_fetch_array($result))
        {
            $value =  $fetched[0];
            if (!in_array($value, $excludes))
            {
                $tables[] = $value;
            }
        }
        mysql_close();
        return $tables;
    }

    private function add_database_to_backup($configuration)
    {
        $user     = $configuration['user'];
        $password = $configuration['password'];
        $database = $configuration['database'];
        $tables = $this->get_tables($configuration);
        $this->execute(MYSQLDUMP." -u ".$user." -p".$password." --skip-extended-insert --single-transaction ".$database." ".implode(" ", $tables)." > ".$database.".sql");
        $this->execute(GIT." add ".$database.".sql");
        return $database.".sql";
    }

    private function add_directory_to_backup($configuration)
    {
        $directory = rtrim($configuration['directory'], "/");

        $targetDirectory = substr($directory, strrpos($directory, "/") + 1);
        // we need to exclude git repositories 
        // since they would prevent a real backup 
        // of all files due to respect of .gitignore
        // additionally we would backup most things multiple
        // times through their stored versions in .git
        $gitExclude = array('.git', '.gitignore');
        $exclude = $this->get_excludes($configuration);
        $exclude = array_merge($exclude, $gitExclude);

        $this->execute(RSYNC." -au --delete ".$directory." . --exclude=".implode(" --exclude=", $exclude));
        $this->execute(GIT." add ".$targetDirectory);
        return $targetDirectory;
    }

    private function store($backupped)
    {
        $stamp = new DateTime('NOW');
        $this->execute(GIT." checkout -b ".$stamp->format("Y-m-d\TH-i-sO"));
        $this->execute(GIT." commit -m \"Backup ".$stamp->format(DateTime::ISO8601)."\"");
        $this->execute(GIT." checkout -f master");
    }

    private function add_tracked_items_to_backup()
    {
        $backupped = array();
        foreach ($this->config('database', array()) as $name => $configuration)
        {
            $this->print_info("Preparing database \"".$name."\"");
            $backupped[$name] = $this->add_database_to_backup($configuration);;
            $this->print_info("Preparing database \"".$name."\" finished");
        }
        foreach ($this->config('filesystem', array()) as $name => $configuration) 
        {
            $this->print_info("Preparing filesystem \"".$name."\"");
            $backupped[$name] = $this->add_directory_to_backup($configuration);
            $this->print_info("Preparing filesystem \"".$name."\" finished");
        }
        return $backupped;
    }

    private function remove_untracked_items_from_backup($backupped)
    {
        $backuppedEntries = array_values($backupped);
        $directory = dir(".");
        $entries = array();
        while (false !== ($entry = $directory->read())) {
            if ($entry != ".." && $entry != "." && $entry != ".git" && !in_array($entry, $backuppedEntries))
            {
                $this->execute(GIT." rm ".$entry);
                $entries[] = $entry;
            }
        }
        $directory->close();
    }

    private function create_incremental_backup()
    {
        $backupped = $this->add_tracked_items_to_backup();
        $this->remove_untracked_items_from_backup($backupped);
        $this->store($backupped);
        $this->print_info("Backup finished");
    }

    private function delete_oldest_backup()
    {
        $branch = $this->get_oldest_backup();
        if (!empty($branch))
        {
            $this->print_info("Removing obsolete backup \"".$branch."\"");
            $this->execute("git branch -D ".$branch);
        }
    }

    private function delete_obsolete_backups()
    {
        if ($this->config('rewinds', 0) > 0)
        {
            while ($this->number_of_backups() > $this->config("rewinds"))
            {
                $this->delete_oldest_backup();
            }
        }
    }
}
?>