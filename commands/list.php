<?php 

require_once("_repository.php");

class ListCommand extends RepositoryCommand
{
    public function list_all_backups()
    {
        $backups = $this->get_backups();
        foreach ($backups as $backup)
        {
            echo "\t".$backup.PHP_EOL;
        }
    }  

    public function run_in_repository()
    {
        $this->list_all_backups();
    }
}

?>