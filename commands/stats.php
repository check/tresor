<?php 

require_once("_repository.php");

class StatsCommand extends RepositoryCommand
{
    protected function considered_databases()
    {
        if ($this->has('database'))
        {
            $databases = "";
            foreach ($this->config('database') as $name => $database)
            {
                $databases .= $name." (".$database['name'].") ";
            }
            return $databases;
        }
        return "none";
    }

    protected function considered_filesystems()
    {
        if ($this->has('filesystem'))
        {
            $filesystems = "";
            foreach ($this->config('filesystem') as $name => $filesystem)
            {
                $filesystems .= $name." (".$filesystem['path'].") ";
            }
            return $filesystems;
        }
        return "none";
    }

    protected function store_directory()
    {
        // works because we just changed into the backup store 
        return getcwd();
    }

    protected function remote_directory()
    {
        $split = strrpos($this->config('remote'), ":");

        return substr($this->config('remote'), $split + 1)." (".substr($this->config('remote'), 0, $split).")";
    }

    protected function flag($flag)
    {
        return $this->config($flag) ? "activated" : "deactivated";
    }

    protected function print_statistics()
    {
        $isMirror = $this->has('remote');
        echo "==== ".($isMirror ? "Backup mirror" : "Backup master").PHP_EOL;
        if ($isMirror)
        {
            echo "Remote:      ".$this->remote_directory().PHP_EOL;
        }
        echo "Local:       ".$this->store_directory().PHP_EOL;
        echo "== Options".PHP_EOL;
        echo "\tCompression: ".$this->flag('compress').PHP_EOL;
        echo "\tCleanup:     ".$this->flag('empty_temporary_storage').PHP_EOL;
        echo "== Storage".PHP_EOL;
        $numberOfBackups = $this->number_of_backups();
        if ($isMirror)
        {
            echo "\tRepository contains ".$numberOfBackups." backups".PHP_EOL;
        }
        else
        {
            echo "\tRepository contains ".$numberOfBackups." of ".$this->config('rewinds')." backups".PHP_EOL;
        }
        $sizeOfBackups = trim($this->execute(DU." -sh .git"));
        $sizeOfCopies = trim($this->execute(DU." -sh ."));
        echo "\tusing   ".substr($sizeOfBackups, 0, strrpos($sizeOfBackups, "\t"))." disk space".PHP_EOL;
        echo "\toverall ".substr($sizeOfCopies, 0, strrpos($sizeOfCopies, "\t"))." disk space".PHP_EOL;
        echo "\tOldest backup: ".$this->get_oldest_backup().PHP_EOL;

    }

    public function run_in_repository()
    {
        $this->print_statistics();
    }
}

?>