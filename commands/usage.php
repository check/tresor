<?php

require_once("_command.php");

class UsageCommand extends Command
{
    private $application;

    public function __construct($application, $configuration)
    {
        parent::__construct($configuration);
        $this->application = $application;
    }

    public function run()
    {
        $this->print_usage();
    }

    public function print_usage()
    {
        echo "Usage: ".$this->application." <command> [arguments] [options]".PHP_EOL;
        echo "  Commands:".PHP_EOL;
        echo "    list                        lists all stored backups".PHP_EOL;
        echo "    backup                      stores a new backup in the repository".PHP_EOL;
        echo "    restore <backup>            restores the given backup inside the store directory".PHP_EOL;
        echo "    snapshot <backup>           stores a full backup of the given backup on the given path".PHP_EOL;
        echo "    sync                        syncs the whole backup with the configured remote repository (only usable on mirror repository)".PHP_EOL;
        echo "    check                       checks for updates in the remote repository (only usable on mirror repository)".PHP_EOL;
        echo "    stats                       retrieves the statistics of the local backup repository".PHP_EOL;
        echo "    help                        print this help".PHP_EOL;
        echo "  Options:".PHP_EOL;
        echo "    -c|--configuration <path>   use the configuration specified by path (defaults to configuration.json in the current working directory".PHP_EOL;
        echo "                                accepts legacy php format".PHP_EOL;
    }
}
?>