# Tresor - a simple data vault

Tresor is a very simple data store written in _PHP_ making use of the powerful features of _git_, _rsync_ and _mysqldump_. The target of Tresor is to keep backups of files and database tables (only mysql). In fact, it just collects data using rsync and mysqldump and commits them into an git repository. It is barely tested (shame on me) and not written to support other systems than Linux. If you need features, please create an issue or fork Tresor and send me a pull request.

Just to be sure - here is what Tresor is not:

- a full fledged backup and restore application - it just collects data and stores it in a git repository
- an automatic backup application - you may use cron jobs since it is just a script
- bugfree - it is only successfully tested in one scenario - the one it was written for ;) but in case you stumble upon one, I will hunt it down
- complicated - it just wants to aggregate common tools in a helpful way to fulfill our needs - therefore most options are truely optinal and the configuration should be easy enough when starting with an example configuration file

And now: What can you do with Tresor?

- backup specific paths of your filesystem
- backup specific databases of your MySQL server
- exclude folders and tables from your backup using the exclude property
- hold the data in a single location with compression and efficient duplicate handling
- using _cron_ you can create periodic backup jobs to a local backup repository
- synchronizing backups of a remote repository using ssh or a local filesystem

To see how to use Tresor, execute the _run.php_ script which lists the commands with a short description.

There is still a lot thing which can improve:

- include the important parts of the remote configuration to allow for easier resyncronization 
- allow for empty directories/tables instead of completely excluding them
- create a deamon or start cronjobs from within Tresor instead of relying on the user
- improved readability for the backups (some easier to read date time format)
- optional feature to deny push to the master repository from mirrors
- optional feature to log accesses to the master repository and pulls on the mirror repository
- a website for controlling Tresor as alternative interface to the command line
- exclude different versioning systems from backups since this would simply double the whole thing
- pull backups from remote repository in chunks (git fetch --depth=n) to allow for large backups
- pull only a selection of backups (each n-th backup) for mirrors
- determine number of backups considering the size of the backup store (ensure that a minimal number of backups exists)

There is also more technical stuff to play around: 
    
- git config pack.windowMemory "100m"
- git config pack.packSizeLimit "100m"
- git config pack.threads "1"

Requirements:
- PHP with mysql support 
- mysqldump 
- git 
- rsync
- du
- touch