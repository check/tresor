#!/usr/bin/php
<?php

define("DEBUG", 0);

date_default_timezone_set('UTC');

require_once("tools.php");

function parse($arguments)
{
    $parsed = array(
        'application' => $arguments[0],
        'command' => (sizeof($arguments) > 1 ? $arguments[1] : "help"),
        'configuration' => getcwd()."/configuration.json",
        'arguments' => array(),
    );
    for ($i = 2; $i < sizeof($arguments); ++$i)
    {
        if (strpos($arguments[$i], "--configuration") !== false
            || strpos($arguments[$i], "-c") !== false)
        {
            if (strpos($arguments[$i], "=") !== false)
            {
                $parsed['configuration'] = substr($arguments[$i], strpos($arguments[$i], "=") + 1);
            }
            else
            {
                $parsed['configuration'] = $arguments[$i + 1];
                $i++;
            }
        }
        else
        {
            $parsed['arguments'][] = $arguments[$i];
        }
    }
    return $parsed;
}

function default_configuration()
{
    return array(
        'compress' => false,
        'empty_temporary_storage' => false,
        'verbose' => 0,
        'dry_run' => false,
        'arguments' => array()
    );
}

function load_configuration($file)
{
    if (file_exists($file))
    {
        if (stripos($file, ".php", strlen($file) - 5) !== false)
        {
            include($file);
            return $backup;
        }
        if (stripos($file, ".json", strlen($file) - 5) !== false)
        {
            $content = file_get_contents($file);
            $content = preg_replace('/\w*#.*/', "", $content);
            return json_decode($content, true);
        }
        throw new Exception("Cannot process configuration file '".$file."' because of unknown format");
    }
    throw new Exception("Cannot find configuration file '".$file."'");
}

function get_configuration($parsed)
{
    $configuration = default_configuration();
    $backup = load_configuration($parsed['configuration']);

    foreach ($backup as $option => $value)
    {
        $configuration[$option] = $value;
    }
    if (strpos($configuration['store'], '/') != 0)
    {
        $configuration['store'] = substr($parsed['configuration'], 0, strrpos($parsed['configuration'], "/"))."/".$configuration['store'];
    }
    $configuration['arguments'] = $parsed['arguments'];
    return $configuration;
}

function dispatch_for_command($arguments)
{
    $parsed = parse($arguments);
    $configuration = get_configuration($parsed);
    switch ($parsed['command']) {
        case 'backup':
            require_once("commands/backup.php");
            return new BackupCommand($configuration);
        case 'snapshot':
            require_once("commands/snapshot.php");
            return new SnapshotCommand($configuration);
        case 'restore':
            require_once("commands/restore.php");
            return new RestoreCommand($configuration);
        case 'list':
            require_once("commands/list.php");
            return new ListCommand($configuration);
        case 'sync':
            require_once("commands/sync.php");
            return new SyncCommand($configuration);
        case 'stats':
            require_once("commands/stats.php");
            return new StatsCommand($configuration);
        case 'check':
            require_once("commands/check.php");
            return new CheckCommand($configuration);
        default:
            require_once("commands/usage.php");
            return new UsageCommand($application, $configuration);
    }
}

try
{
    $command = dispatch_for_command($argv);
    $command->run();
}
catch (Exception $e)
{
    echo $e->getMessage().PHP_EOL;
    if (DEBUG > 0)
    {
        echo $e->getTraceAsString().PHP_EOL;
    }
    require_once("commands/usage.php");
    $usage = new UsageCommand($argv[0], array());
    $usage->run();
}
?>