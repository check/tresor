<?php

// adjust these paths if you don't want to use the standard installations of the used tools
define("GIT", "git");
define("RSYNC", "rsync");
define("MYSQLDUMP", "mysqldump");
define("TOUCH", "touch");
define("DU", "du");

?>